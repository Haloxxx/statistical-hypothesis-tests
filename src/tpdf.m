function y = tpdf(x, v)

  if (nargin < 2)
    print_usage ();
  endif

  
  y = gamma((v+1)/2)/(gamma(v/2)*sqrt(v*pi)).*(1+x.^2./v).^(-(v+1)/2);
endfunction