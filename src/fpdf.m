function y = fpdf(x, d1, d2)

  if (nargin < 2)
    print_usage ();
  endif

  y = (1/beta(d1/2,d2/2))*(d1/d2)^(d1/2)*x.^(d1/2-1).*(1+(d1/d2)*x).^(-(d1+d2)/2);
  #y = (1/beta(d1/2,d2/2))*(d1/d2)^(d1/2);
endfunction