function y = fcdf(x, d1, d2)

  if (nargin < 2)
    print_usage ();
  endif

  x = d1.*x./(d1.*x+d2);
  y = betainc(x, d1/2, d2/2);
endfunction