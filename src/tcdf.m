function y = tcdf(t, v)

  if (nargin < 2)
    print_usage ();
  endif
  
  x = [];
  YM = [];
  YP = [];
  
  for i = t
    x = v/(i^(2)+v);
    if i < 0
      YM = [YM; 1 - (1/2)*betainc(0, v/2, 1/2) - (1 - (1/2)*betainc(x, v/2, 1/2))];
    endif
    if i >= 0
      YP = [YP; 1 - (1/2)*betainc(x, v/2, 1/2)];
    endif
  endfor
  
  y = [YM; YP];

endfunction