function y = tinv(x, v)

  if (nargin < 2)
    print_usage ();
  endif

  y = (sign(x - 1/2).*sqrt(v./betainv(2*min(x, 1-x), v/2, 1/2) - v));
endfunction