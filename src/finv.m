function y = finv(x, d1, d2)

  if (nargin < 2)
    print_usage ();
  endif

  y = (d1/d2).*(1./(betainv(1 - x, d1/2, d2/2))-1);
endfunction