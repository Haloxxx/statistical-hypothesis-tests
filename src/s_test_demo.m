x = -4:0.01:4;

plot (x, tpdf(x, 1));
hold on
plot (x, tpdf(x, 2));
plot (x, tpdf(x, 5));
plot (x, tpdf(x, 10));
hold off

waitfor(gcf)

plot (x, tcdf(x, 1))
hold on
plot (x, tcdf(x, 2))
plot (x, tcdf(x, 5))
plot (x, tcdf(x, 10))
hold off

waitfor(gcf)

x = 0:0.01:5;
plot (x, fpdf(x, 1, 1))
hold on
plot (x, fpdf(x, 2, 1))
plot (x, fpdf(x, 5, 2))
plot (x, fpdf(x, 10, 1))
plot (x, fpdf(x, 100, 100))
hold off

waitfor(gcf)

plot (x, fcdf(x, 1, 1))
hold on
plot (x, fcdf(x, 2, 1))
plot (x, fcdf(x, 5, 2))
plot (x, fcdf(x, 100, 1))
plot (x, fcdf(x, 100, 100))
hold off
waitfor(gcf)